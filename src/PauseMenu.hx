import dn.Process; 
import hxd.Key;

import ui.Components;

@:uiComp("pause-menu")
class PauseMenuContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <pause-menu>
		<button public id="cont"/>
		<button public id="restart"/>
		<button public id="config"/>
		<button public id="exit"/>
	</pause-menu>;

	public function new(align:h2d.Flow.FlowAlign, ?parent) {
		super(parent);
		initComponent();
	}

}


class PauseMenu extends dn.Process {
	var ca : dn.heaps.Controller.ControllerAccess;
	var mask : h2d.Bitmap;

	var center : h2d.Flow;
	var style = null;

	public function new() {
		super(Main.ME);

		createRootInLayers(Main.ME.root, Const.DP_UI);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering
		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "PAUSE - press Escape to resume";

		ca = Main.ME.controller.createAccess("modal", true);
		Game.ME.pause();

		mask = new h2d.Bitmap(h2d.Tile.fromColor(0x0, 1, 1, 0.6), root);
		root.under(mask);

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( w()*0.35 / tf.textWidth )) );
					tf.x = Std.int( w()*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( h()*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		center = new h2d.Flow(root);
		center.horizontalAlign = center.verticalAlign = Middle;
		onResize();

		var root = new PauseMenuContainer(Right, center);

		// Override
		root.cont.label = "Continue";
		root.restart.label = "Restart";
		root.config.label = "Config";
		root.exit.label = "Exit";

		root.cont.onClick = function() {
			close();
		}

		root.restart.onClick = function() {
			Game.ME.restartLevel();
			close();
		}

		root.config.onClick = function() {
			ca.releaseExclusivity();
			new ConfigMenu(this);
		}

		root.exit.onClick = function() {
			Main.ME.restart();
			close();
		}


		style = new h2d.domkit.Style();
		style.load(hxd.Res.style);
		style.addObject(root);

		dn.Process.resizeAll();
	}

	override function resume(){
		super.resume();

		ca.takeExclusivity();
	}

	override function onResize() {
		super.onResize();

		root.scale(Const.UI_SCALE);

		mask.scaleX = M.ceil( w()/Const.UI_SCALE );
		mask.scaleY = M.ceil( h()/Const.UI_SCALE );

		center.minWidth = center.maxWidth = w();
		center.minHeight = center.maxHeight = h();
	}

	override function onDispose() {
		super.onDispose();
		ca.dispose();
		Game.ME.resume();
	}

	public function close() {
		if( !destroyed ) {
			destroy();
		}
	}

	override function update() {
		super.update();

		style.sync();

		if( ca.startPressed() || ca.isKeyboardPressed(hxd.Key.ESCAPE) )
			close();
	}
}
