import dn.Process;
import hxd.Key;

class Game extends Process {
	public static var ME : Game;

	public var ca : dn.heaps.Controller.ControllerAccess;
	public var fx : Fx;
	public var camera : Camera;
	public var scroller : h2d.Layers;
	public var level : Level;
	public var hud : ui.Hud;
	public var hero : ent.Hero;

	public var world : World;
	public var curLevelIdx : Int;

	public function new() {
		super(Main.ME);
		ME = this;
		ca = Main.ME.controller.createAccess("game");
		ca.setLeftDeadZone(0.2);
		ca.setRightDeadZone(0.2);
		createRootInLayers(Main.ME.root, Const.DP_BG);

		scroller = new h2d.Layers();
		root.add(scroller, Const.DP_BG);
		scroller.filter = new h2d.filter.ColorMatrix(); // force rendering for pixel perfect

		engine.backgroundColor = 0xeeeeee;

		curLevelIdx = 0;
		world = new World( hxd.Res.world.world.entry.getText() );

		fx = new Fx();
		hud = new ui.Hud();

		startLevel();

		Process.resizeAll();
	}

	private function startLevel() {
		camera = new Camera();
		level = new Level(world.levels[curLevelIdx]);
		level.initLevel();
		hero = new ent.Hero(level.spawnX, level.spawnY);
		camera.trackTarget(hero, false);
	}
	
	public function nextLevel() {
		curLevelIdx++;
		if (curLevelIdx < world.levels.length)
			restartLevel();
		else
			Main.ME.end();
	}

	public function onCdbReload() {
	}

	override function onResize() {
		super.onResize();
		scroller.setScale(Const.SCALE);
	}

	public function respawn() {
		var remainingSeeds = hero.remainingSeeds;
		if (level.isFlipped)
			restartLevel();
		hero.destroy();
		var b = level.getBlock(level.spawnX, level.spawnY);
		if (b != null)
			b.destroy();
		hero = new ent.Hero(level.spawnX, level.spawnY, remainingSeeds);
		camera.trackTarget(hero, true);
	}

	public function restartLevel() {

		engine.backgroundColor = 0xeeeeee;

		for(e in Entity.ALL)
			e.destroy();
		hero.destroy();
		level.destroy();
		camera.destroy();
		gc();
		startLevel();
	}


	function gc() {
		if( Entity.GC==null || Entity.GC.length==0 )
			return;

		for(e in Entity.GC)
			e.dispose();
		Entity.GC = [];
	}

	override function onDispose() {
		super.onDispose();

		fx.destroy();
		camera.destroy();
		for(e in Entity.ALL)
			e.destroy();
		level.destroy();
		gc();
	}

	override function preUpdate() {
		super.preUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.preUpdate();
	}

	override function postUpdate() {
		super.postUpdate();



		for(e in Entity.ALL) if( !e.destroyed ) e.postUpdate();
		gc();
	}

	override function fixedUpdate() {
		super.fixedUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.fixedUpdate();
	}

	override function update() {
		super.update();


		for(e in Entity.ALL) if( !e.destroyed ) e.update();

		if( !ui.Console.ME.isActive() && !ui.Modal.hasAny() ) {
			if( ca.isKeyboardPressed(Key.ESCAPE) || ca.startPressed() )
				new PauseMenu();
		}
	}
}

