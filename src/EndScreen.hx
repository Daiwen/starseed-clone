import ui.Components;

@:uiComp("end-screen")
class EndContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <end-screen>
		</end-screen>;

	public function new(align:h2d.Flow.FlowAlign, ?parent) {
		super(parent);
		initComponent();
	}

}

class EndScreen extends dn.Process {
	var img : h2d.Bitmap;

	var center : h2d.Flow;

	public function new() {
		super(Main.ME);

		createRootInLayers(Main.ME.root, Const.DP_UI);
		img = new h2d.Bitmap(hxd.Res.title.toTile(), root);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering

		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "Thanks for playing !";

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( w()*0.35 / tf.textWidth )) );
					tf.x = Std.int( w()*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( h()*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		center = new h2d.Flow(root);
		center.horizontalAlign = center.verticalAlign = Middle;
		onResize();

		var root = new EndContainer(Middle, center);

		dn.Process.resizeAll();
	}

	override function onDispose() {
		super.onDispose();
	}

	override function onResize() {
		super.onResize();

		img.x = ( w()/Const.SCALE*0.5 - img.tile.width*0.5 );
	}

	override function postUpdate() {
		super.postUpdate();
	}

	override function update() {
		super.update();
	}
}
