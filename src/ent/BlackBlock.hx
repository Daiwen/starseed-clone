package ent;

import ent.Types.BlackBlockType;
import haxe.ds.Option;

class BlackBlock extends Entity {

	public static var ALL : Array<BlackBlock> = [];

	var type : BlackBlockType;

	var decoration : h2d.Object;

	public function new (x, y, ?type=Void) {
		super(x, y);

		isFixed = true;

		game.scroller.add(spr, Const.DP_BG);

		var g = new h2d.Graphics(spr);
		g.beginFill(0x000000);
		g.drawRect(-Const.GRID/2,-Const.GRID,Const.GRID,Const.GRID);
		level.addBlackBlock(x, y);
		ALL.push(this);

		this.type = type;

		switch (type) {
			case Key | Lock:
				decoration = new h2d.Object();
				game.scroller.add(decoration, Const.DP_BG);
				Assets.tiles.getBitmap("qmark", 0, 0.5, 0, decoration);

				decoration.x = Std.int(headX);
				decoration.y = Std.int(headY);

			case StrongLock:
				decoration = new h2d.Object();
				game.scroller.add(decoration, Const.DP_BG);
				Assets.tiles.getBitmap("qmark3", 0, 0.5, 0, decoration);

				decoration.x = Std.int(headX);
				decoration.y = Std.int(headY);

			case Void:
		}
	}

	override function dispose() {
		super.dispose();
		decoration.remove();

		ALL.remove(this);
	}

	override function update() {
		super.update();

		infectNeighbour(  cx, cy-1);
		infectNeighbour(  cx, cy+1);
		infectNeighbour(cx+1,   cy);
		infectNeighbour(cx-1,   cy);

	}

	function infectNeighbour(cx, cy) {
		var block = level.getBlock(cx, cy);
		if (block != null)
			block.turnBlack();

		var eblock = level.getEmptyBlock(cx, cy);
		if (eblock != null)
			eblock.turnBlack();
	}

	public function flip(){
		var g = new h2d.Graphics(spr);
		g.beginFill(0xffffff);
		g.drawRect(-Const.GRID/2,-Const.GRID,Const.GRID,Const.GRID);

		switch (type) {
			case Lock:
				new ent.Lock(cx, cy);
			case StrongLock:
				new ent.Lock(cx, cy, 3);
			case Key:
				new ent.Key(cx, cy);
			case Void:
		}
	}

}
