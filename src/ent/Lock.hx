package ent;

import haxe.ds.Option;

class Lock extends Entity {

	public static var ALL : Array<Lock> = [];

	var decoration : h2d.Object;

	var locks : Int;

	public function new (x, y, ?locks = 1) {
		super(x, y);

		decoration = new h2d.Object();
		game.scroller.add(decoration, Const.DP_BG);
		Assets.tiles.getBitmap("lock", 0, 0.5, 0, decoration);


		var tf = new h2d.Text(Assets.fontPixel, decoration);
		tf.text = "" + locks;
		tf.x = -Std.int( tf.textWidth );
		tf.y = 64;
		tf.setScale(2);

		ALL.push(this);

		this.locks = locks;

		decoration.x = Std.int(headX);
		decoration.y = Std.int(headY);
	}

	override function dispose() {
		super.dispose();
		decoration.remove();

		ALL.remove(this);
	}

	override function postUpdate() {
		super.postUpdate();

		if (onGround && !isFixed) {
			cy += 1;

			level.addLock(this);
			isFixed = true;
		}

		if (decoration != null) {
			decoration.x = Std.int(headX);
			decoration.y = Std.int(headY);
		}

	}

	public function open(keyNumber : Int) {
		if (keyNumber >= locks && locks == 3)
			game.nextLevel();
		else if (keyNumber >= locks)
			game.respawn();
		else
			game.restartLevel();
	}

}
