package ent;

import haxe.ds.Option;

class EmptyBlock extends Entity {

	public static var ALL : Array<EmptyBlock> = [];

	var isInfected : Bool;

	public function new (x, y) {
		super(x, y);

		game.scroller.add(spr, Const.DP_BG);


		level.addEmptyBlock(this);
		ALL.push(this);
		isFixed = true;
		var g = new h2d.Graphics(spr);
		g.beginFill(0xe4e4e4);
		g.drawRect(-Const.GRID/2,-Const.GRID,Const.GRID,Const.GRID);

		var isInfected = false;
	}

	override function dispose() {
		super.dispose();

		level.removeEmptyBlock(cx, cy);
		ALL.remove(this);
	}

	override function postUpdate() {
		super.postUpdate();

		if (isInfected)	{
			new BlackBlock(cx, cy);
			destroy();
		}
	}

	public function turnBlack(){
		isInfected = true;
	}

}
