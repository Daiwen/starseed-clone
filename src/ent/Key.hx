package ent;

import haxe.ds.Option;

class Key extends Entity {

	public static var ALL : Array<Key> = [];

	var decoration : h2d.Object;


	public function new (x, y) {
		super(x, y);

		decoration = new h2d.Object();
		game.scroller.add(decoration, Const.DP_BG);
		Assets.tiles.getBitmap("key", 0, 0.5, 0, decoration);

		var m = new h3d.Matrix();
		m.identity();
		m.colorSet(0x000000, 1);
		decoration.filter = new h2d.filter.ColorMatrix(m);

		ALL.push(this);

		decoration.x = Std.int(headX);
		decoration.y = Std.int(headY);
	}

	override function dispose() {
		super.dispose();
		decoration.remove();

		ALL.remove(this);
	}

	override function update() {
		super.update();
	}

	override function postUpdate() {
		super.postUpdate();


		if (decoration != null) {
			decoration.x = Std.int(headX);
			decoration.y = Std.int(headY);
		}

	}

}
