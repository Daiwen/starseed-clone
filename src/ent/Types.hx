package ent;

enum Direction {
	Right;
	Left;
	Up;
	Down;
}

enum Seed {
	Pink;
	Orange;
	Purple;
}


enum BlockType {
	Seedable(s:Seed);
	Brown;
}

enum BlackBlockType {
	Key;
	Lock;
	StrongLock;
	Void;
}

class Util {
	static public function getSeedId(s : Seed) {
		var id =
			switch (s) {
				case Pink:
					"pink";
				case Orange:
					"orange";
				case Purple:
					"purple";
			}

		return id;
	}

	static public function getSeedColor(s : Seed) {
		var color =
		switch (s) {
			case (Pink):
				0xff67f8;
			case (Orange):
				0xd37800;
			case (Purple):
				0x9b2eff;
		}

		return color;
	}

	static public function getBlockColor(b : BlockType) {
		var color =
			switch (b) {
				case Seedable(s):
					getSeedColor(s);
				case Brown:
					0x9a5d00;
			}

		return color;
	}
}
