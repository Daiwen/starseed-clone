package ent;
import hxd.Key;
import ent.Types.Seed;

enum State {
	Stand;
	Run;
}

class Hero extends Entity {
	public static var ME : Null<Hero>;
	var ca : dn.heaps.Controller.ControllerAccess;

	var state: State;

	public var remainingSeeds: Int;
	var bufferSize: Int;

	var seeds: Array<Seed> = [];
	var seedDisplay: Null<h2d.Object>;
	var shouldRedraw : Bool;

	var keys : Int;

	private function isRunning() {
		var res =
		switch (state) {
			case Run: true;
			default: false;
		};

		return res;
	}

	private function getRandomSeed() {
		if (remainingSeeds == 0)
			return;

		var r = M.rand(3);
		var s =
			if (r == 0)
				Pink;
			else if (r == 1)
				Orange;
			else
				Purple;


		seeds.push(s);
		remainingSeeds--;

		shouldRedraw = true;
		return;
	}


	public function new (x, y, ?remainingSeeds=10) {
		super(x, y);

		ME = this;

		state = Stand;

		spr.anim.registerStateAnim("heroRun", 2, 0.25, function() return state == Run);
		spr.anim.registerStateAnim("heroStand", 2, 0.05, function() return state == Stand);

		bufferSize = 3;

		keys = 0;

		this.remainingSeeds = remainingSeeds;

		ca = Main.ME.controller.createAccess("hero");
	}

	override function dispose() {
		super.dispose();
		ca.dispose();
		clearSeedDisplay();
	}

	override function update() {
		super.update();


		var dirx = ca.lxValue();

		state =
			if (Math.abs(dirx) > 0.05) {
				dx = dirx * 0.05 * tmod;
				Run;
			} else {
				Stand;
			}


		if (dirx > 0.3)
			dir = -1;
		else if (dirx < -0.3)
			dir = 1;


		if ( onGround )
			cd.setS("onGroundRecently",0.1);

		//Jump
		if(  ( ca.aPressed() && cd.has("onGroundRecently") ) ) {
			setSquashX(0.9);
			dy = -0.07;
			cd.setS("jumpForce",0.6);
			cd.setS("jumpExtra",0.6);
		}
		else if( cd.has("jumpExtra") && ca.aDown() )
			dy-=0.04*tmod;

		if( cd.has("jumpForce") && ca.aDown() )
			dy -= 0.05 * cd.getRatio("jumpForce") * tmod;

		if (onGround || dy < 0)
			cd.setS("fallSquash", 1);
		if (!onGround && dy > 0)
			setSquashX( 1 - 0.1 * (1 - cd.getRatio("fallSquash")));

		//Seeds
		if (ca.isKeyboardPressed(Key.SPACE) && !cd.has("seedcd")) {
			plantSeed();
			cd.setS("seedcd", 0.4);
		}


		if ((ca.isKeyboardDown(Key.S) || ca.isKeyboardDown(Key.DOWN)) && !cd.has("breakblock")) {
			if (level.isFlipped) {
				var l = level.getLock(cx, cy + 1);
				l.open(keys);
			}
			else
				breakBlock(0, 1);
		}
		else if(dy == 0) {
			if((ca.isKeyboardDown(Key.D) || ca.isKeyboardDown(Key.RIGHT)) && !cd.has("breakblock"))
				breakBlock(1, 0);
			else if((ca.isKeyboardDown(Key.Q) || ca.isKeyboardDown(Key.A) || ca.isKeyboardDown(Key.LEFT)) && !cd.has("breakblock"))
				breakBlock(-1, 0);
		}

		//Keys
		for (k in ent.Key.ALL) {
			if (k.cx == cx && k.cy == cy) {
				k.destroy();
				keys++;
				shouldRedraw = true;
			}
		}
	}

	private function breakBlock(dx, dy) {
		cd.setS("breakblock", 0.5);
		var b = level.getBlock(cx+dx, cy+dy);
		if (b != null) {
			b.broken();
			b.destroy();
		}
	}

	public function pickUpSeed() {
		remainingSeeds++;
		shouldRedraw = true;
	}

	private function plantSeed() {
		if (seeds.length > 0 && onGround) {
			var b = level.getBlock(cx, cy + 1);
			if (b.plant(seeds[0])) {
				seeds.remove(seeds[0]);
				shouldRedraw = true;
			}
		}
	}


	function updateSeedDisplay() {
		clearSeedDisplay();
		seedDisplay = new h2d.Object();
		game.scroller.add(seedDisplay, Const.DP_UI);

		if (level.isFlipped) {
			for (i in 0...keys) {
				var b = Assets.tiles.getBitmap("key", 0, 0.5, 1 + i*0.75, seedDisplay);
				b.setScale(0.5);

				var g1 = new h2d.filter.Glow(0xFFFFFF, 100, 2);
				g1.knockout = true;


				var m = new h3d.Matrix();
				m.identity();
				m.colorSet(0x000000, 1);
				var g2 = new h2d.filter.ColorMatrix(m);

				seedDisplay.filter = new h2d.filter.Group([g1, g2]);
			}
		} else {
			for (i in 0...seeds.length) {
				var seedId = ent.Types.Util.getSeedId(seeds[i]);
				Assets.tiles.getBitmap(seedId, 0, 0.7, 2 + i*1.5, seedDisplay);
			}

			var tf = new h2d.Text(Assets.fontPixel, seedDisplay);
			tf.text = "" + remainingSeeds;
			tf.x = -Std.int( tf.textWidth );
			tf.y = 64;
			tf.setScale(2);
		}

		shouldRedraw = false;
	}

	function clearSeedDisplay() {
		if (seedDisplay != null) {
			seedDisplay.remove();
			seedDisplay = null;
		}
	}

	override function postUpdate() {
		super.postUpdate();
		if (!game.camera.isOnScreen(this)) {
			if (!level.isFlipped)
				game.respawn();
			else
				game.restartLevel();
		}

		if (onGround || dy < 0)
			cd.setS("cameraTracking", 0.5);

		if (!onGround && !cd.has("cameraTracking"))
			game.camera.stopTracking();


		if (onGround && game.camera.target == null)
			game.camera.trackTarget(this, false);

		if (seeds.length < bufferSize) {
			getRandomSeed();
		}

		if (shouldRedraw)
			updateSeedDisplay();

		if (seedDisplay != null) {
			seedDisplay.x = Std.int(headX);
			seedDisplay.y = Std.int(headY);
		}

		if (level.hasBlackBlock(cx, cy))
			level.flip();

	}

	public function flip() {
		shouldRedraw = true;
	}
}
