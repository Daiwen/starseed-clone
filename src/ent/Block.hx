package ent;
import ent.Types.Seed;
import ent.Types.BlockType;
import haxe.ds.Option;

class Block extends Entity {

	public static var ALL : Array<Block> = [];

	var type: BlockType;
	var seed: Null<Seed>;
	var seedDisplay: Null<h2d.Object>;
	var blockSpawners: Array<ent.BlockSpawner>;

	var isInfected : Bool;
	var isBroken : Bool;

	public function getInfectTime() {
		var bdata =
			switch (type) {
				case Seedable (Pink):
					Data.blocks.get(pink);
				case Seedable (Orange):
					Data.blocks.get(orange);
				case Seedable (Purple):
					Data.blocks.get(purple);
				case Brown:
					Data.blocks.get(brown);
			}

		return bdata.infectionTime;
	}


	public function new (x, y, b:BlockType) {
		super(x, y);

		isFixed = true;
		var g = new h2d.Graphics(spr);
		g.beginFill(ent.Types.Util.getBlockColor(b));
		g.drawRect(-Const.GRID/2,-Const.GRID,Const.GRID,Const.GRID);

		level.addCollision(this);

		var eb = level.getEmptyBlock(x, y);
		if (eb != null)
			eb.destroy();

		blockSpawners = [];
		type = b;
		seed = null;

		isInfected = false;
		isBroken = false;
		ALL.push(this);
	}

	override function dispose() {
		super.dispose();

		switch (type) {
			case (Seedable(Pink)) if (isBroken):
				Game.ME.hero.pickUpSeed();
			default:
		}

		for (bs in blockSpawners)
			bs.destroy();

		clearSeedDisplay();

		level.removeCollision(cx, cy);
		ALL.remove(this);
	}

	override function update() {
		super.update();

		if (seed != null)
			createFirstBlockSpawner();

		if (isInfected && !cd.has("infecting")) {
			new BlackBlock(cx, cy);
			destroy();
		}
	}

	public function plant(s : Seed) {

		if (seed != null)
			return false;

		seed = s;
		updateSeedDisplay();

		return true;
	}

	public function grown(isSeed:Bool) {
		if (isSeed) {
			seed = null;
			clearSeedDisplay();
		}
	}

	function updateSeedDisplay() {
		clearSeedDisplay();
		seedDisplay = new h2d.Object();
		game.scroller.add(seedDisplay, Const.DP_UI);

		if (seed != null) {
			var seedId = ent.Types.Util.getSeedId(seed);
			Assets.tiles.getBitmap(seedId, 0, 0.5, -1.5, seedDisplay);
		}

		seedDisplay.x = Std.int(headX);
		seedDisplay.y = Std.int(headY);
	}

	function clearSeedDisplay() {
		if (seedDisplay != null) {
			seedDisplay.remove();
			seedDisplay = null;
		}
	}

	public function addBlockSpawner(bs: ent.BlockSpawner) {
		blockSpawners.push(bs);
	}

	function createFirstBlockSpawner() {
		switch (seed) {
			case Purple:
				if (!level.hasCollision(cx, cy - 1)) {
					if (!level.isOccupied(cx, cy-1)) {
						new BlockSpawner(cx, cy - 1, seed, this, true, 4);
						seed = null;
					}
				}
			case Pink:
				if (!level.hasCollision(cx, cy - 1)) {
					if (!level.isOccupied(cx, cy - 1)) {
						new BlockSpawner(cx, cy - 1, seed, this, true, -1);
						seed = null;
					}
				}
			case Orange:
				var ncx = cx;
				var ncy = cy;
				if (!level.hasCollision(cx + 1, cy))
					ncx += 1;
				else if (!level.hasCollision(cx - 1, cy))
					ncx -= 1;
				else if (!level.hasCollision(cx, cy - 1))
					ncy -= 1;

				if (!level.isOccupied(ncx, ncy)) {
					new BlockSpawner(ncx, ncy, seed, this, true, 5);
					seed = null;
				}
		}

	}

	public function turnBlack() {
		if (!isInfected) {
			cd.setS("infecting", getInfectTime());
			isInfected = true;

			var g = new h2d.Graphics(spr);
			g.lineStyle(3, 0x000000);
			g.drawRect(-Const.GRID/2,-Const.GRID,Const.GRID,Const.GRID);
		}
	}

	public function broken() {
		isBroken = true;
		new ent.EmptyBlock(cx, cy);
	}
}
